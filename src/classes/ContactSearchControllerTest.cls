@isTest
private class ContactSearchControllerTest {

	@isTest static void testSearchController() {
        Account acc = new Account();
        acc.Name = 'Test Account Name';
        insert acc;
        System.assert(acc != null, true);
          
        Contact cont = new Contact();
        cont.FirstName = 'FirstName';
        cont.LastName = 'LastName';
        cont.Department = 'Test Department';
        cont.AccountId = acc.Id;
        insert cont;
        System.assert(cont != null, true);
        
        PageReference pageRef = Page.ContactSearch;
        Test.setCurrentPage(pageRef);
        ContactSearchController cmc = new ContactSearchController();  
        
        test.startTest();
            Apexpages.currentPage().getParameters().put('sName', 'TestValue');
            Apexpages.currentPage().getParameters().put('oType', 'First Name');
            cmc.searchCont();
            
            Apexpages.currentPage().getParameters().put('oType', 'Last Name');
            cmc.searchCont();
            
            Apexpages.currentPage().getParameters().put('oType', 'Account');
            cmc.searchCont();
            
            Apexpages.currentPage().getParameters().put('oType', 'Technology');
            cmc.searchCont();
            
            cmc.openEditPage();
            
        test.stopTest();
	}

}