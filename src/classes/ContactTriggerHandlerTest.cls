@isTest
private class ContactTriggerHandlerTest {

  @isTest static void testUpdateContact() {
      Account acc = new Account();
      acc.Name = 'Test Account Name';
      insert acc;
      System.assert(acc != null, true);
      
        Contact cont = new Contact();
        cont.FirstName = 'FirstName';
        cont.LastName = 'LastName';
        cont.Department = 'Test Department';
        cont.AccountId = acc.Id;
        insert cont;
        System.assert(cont != null, true);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='asdds@dasddd.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='adsda@addas.com');
        insert u;
        System.assert(u != null, true);
        
        EntitySubscription es = new EntitySubscription();
        es.ParentId = cont.Id;
        es.SubscriberId = u.Id;
        insert es;
        System.assert(es != null, true);
        
        
        
        
        test.startTest();
            cont.Department = 'Test Department2';
            update cont;
        test.stopTest();
        
  }

}