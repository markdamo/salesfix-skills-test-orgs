public class ContactSearchController {
    public List<Contact> contList {get; set;}
    public String soqlQuery;
    public String recId{get; set;}
    public List<String> options {get; set;}
    public ContactSearchController(){
        contList = new List<Contact>();
        
        options = new List<String>();
        if(options.isEmpty()){
            options.add('First Name');
            options.add('Last Name');
            options.add('Account');
            options.add('Technology');
        }
    }
    
    
    public PageReference searchCont(){
        String sName = Apexpages.currentPage().getParameters().get('sName');
        String oType = Apexpages.currentPage().getParameters().get('oType');
        soqlQuery = 'Select Id, Name, FirstName, LastName, Account.Name, Technology__c FROM Contact WHERE Name != null';
        if(!sName.equals('')){
            if(oType == 'First Name'){
                soqlQuery += ' AND FirstName LIKE \'%' + String.escapeSingleQuotes(sName) + '%\'';
            }
            else if(oType == 'Last Name'){
                soqlQuery += ' AND LastName LIKE \'%' + String.escapeSingleQuotes(sName) + '%\'';
            }
            else if(oType == 'Account'){
                soqlQuery += ' AND Account.Name LIKE \'%' + String.escapeSingleQuotes(sName) + '%\'';
            }
            else if(oType == 'Technology'){
                soqlQuery += ' AND Technology__c LIKE \'%' + String.escapeSingleQuotes(sName) + '%\'';
            }
            contList = Database.query(soqlQuery);
        }

        return null;
    }
    
    public PageReference openEditPage(){
        recId = Apexpages.currentPage().getParameters().get('recId');
        System.debug('@@@@: ' +recId);
        return null;
    }
    
}