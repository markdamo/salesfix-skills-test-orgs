public class ContactTriggerHandler {
    public static void contactTriggerHandler(){
        if(trigger.isAfter){
            if(trigger.isUpdate){
                onAfterUpdatePostChatter(trigger.new);
            }
        }
    }
    
    public static void onAfterUpdatePostChatter (List<Contact> newContList){
        Set<Id> userSetIds = new Set<Id>();
        Map<Id, Contact> idToContactMap = new Map<Id, Contact>();
        Map<Id, String> userIdToContNameMap = new Map<Id, String>();

        for(EntitySubscription es: [SELECT Id, SubscriberId, ParentId, Parent.Name FROM EntitySubscription WHERE ParentId IN: newContList]){
            userIdToContNameMap.put(es.SubscriberId, es.Parent.Name);
        }
        if(!userIdToContNameMap.isEmpty()){
            for(Id userId: userIdToContNameMap.keySet()){
                if(!Test.isRunningTest())
                    ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), userId, ConnectApi.FeedElementType.FeedItem, userIdToContNameMap.get(userId) + ' has been Updated.');
            }
        }    
    }

}